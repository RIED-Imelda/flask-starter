FROM python:3.9-slim

EXPOSE 8080

COPY Pipfile .
COPY Pipfile.lock .
COPY api.py .
COPY static .

RUN pip install micropipenv
RUN micropipenv install

ENTRYPOINT waitress-serve --port=8080 --call api:create_app