# Simple Flask Project


## Pre-requisites

- [Python 3.9](https://dev.to/davidshare/setting-up-a-python-development-environment-with-pipenv-3lfj)
- PipEnv `python3 -m pip install pipenv`

You can read more about Pipfile [here](https://www.jetbrains.com/help/pycharm/using-pipfile.html)

## Local development
1. `pipenv install`
2. `pipenv shell`

## Running the application
- python api.py

## Running the tests
- python run_tests.py
